import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
// import ControlledCarousel from "./components/Carouselhome.js";
function Ranking() {
  return (
    <div className="Ranking container-fluid">
      <Container>
        <h1 className="ranktable">Ranking of บ้านทรายทอง Community</h1>
        <h5 className="ranktable">You are 15th of บ้านทรายทอง Community</h5>
        <Table
          striped
          bordered
          hover
          size="lg"
          style={{
            width: "700px",
            marginTop: "50px",
            marginLeft: "200px",
            background: "#fefff2",
          }}
        >
          <thead className="ranktable">
            <tr>
              <th>Name</th>
              <th>Community</th>
            </tr>
          </thead>
          <tbody className="ranktable">
            <tr>
              <td>เชิดชู ชุปปี้</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>จำปี แสงทอย</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>จำปา แสงส่อง</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>เมฆฟ้า ง่วงมาก</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>จำปี แสงทอย</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>จำปี แสงทอย</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>จำปี แสงทอย</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>จำปี แสงทอย</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>จำปี แสงทอย</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>จำปี แสงทอย</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>จำปี แสงทอย</td>
              <td>บ้านทรายทอง</td>
            </tr>
            <tr>
              <td>จำปี แสงทอย</td>
              <td>บ้านทรายทอง</td>
            </tr>
          </tbody>
        </Table>
      </Container>
    </div>
  );
}

export default Ranking;
