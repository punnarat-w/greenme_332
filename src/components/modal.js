import Modal from "react-bootstrap/Modal";
import React from "react";
import Button from "react-bootstrap/Button";
import Scene from "./img/planttree/planting.mp4";
// import VideoPop from "react-video-pop";
import { Player, ControlBar, PlayToggle } from "video-react";

function MyVerticallyCenteredModal(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Watering your tree
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>You can 'Close' when finished.</p>
        <Player autoPlay src={Scene}>
          <ControlBar autoHide={false} disableDefaultControls={true}>
            <PlayToggle />
          </ControlBar>
        </Player>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={props.onHide}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
export default MyVerticallyCenteredModal;
