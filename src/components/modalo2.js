import Modal from "react-bootstrap/Modal";
import React from "react";
import Button from "react-bootstrap/Button";
import Scene from "./img/planttree/recv_o2.mp4";
// import VideoPop from "react-video-pop";
import { Player, ControlBar, PlayToggle } from "video-react";

function MyVerticallyCenteredModalO2(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Watering your tree
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>Congratulations!!! you level is going up now. Here is your Oxygen. Thank for saving the world we will look forward to see you in next level, See ya .You can 'Close' when finished.</p>
        <Player autoPlay src={Scene}>
          <ControlBar autoHide={false} disableDefaultControls={true}>
            <PlayToggle />
          </ControlBar>
        </Player>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={props.onHide}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
export default MyVerticallyCenteredModalO2;
