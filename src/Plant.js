import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Alert from "react-bootstrap/Alert";
import Badge from "react-bootstrap/Badge";
// import AlertTitle from "@material-ui/lab/AlertTitle";
import lv1 from "./components/img/planttree/tree_lv1.png";
import lv2 from "./components/img/planttree/tree_lv2.png";
import lv3 from "./components/img/planttree/tree_lv3.png";
import ProgressBar from "./components/progress-bar.js";
import Button from "react-bootstrap/Button";
import MyVerticallyCenteredModal from "./components/modal.js";
import MyVerticallyCenteredModalO2 from "./components/modalo2.js";
import water from "./components/img/planttree/watertuning.png";
import o2 from "./components/img/planttree/O2.png";
// import ChipScore from "./components/ChipScore.js";
import Amplify, { API } from "aws-amplify";

function Plant() {
  const tree = [lv1, lv2, lv3];
  const [modalShow, setModalShow] = useState(false);
  const [modalOShow, setModalOShow] = useState(false);
  const [completed, setCompleted] = useState(0);
  const [level, setLevel] = useState(1);
  const [numwater, setNumwater] = useState(25);
  const [numoxy, setNumoxy] = useState(20);
  const [alert, setAlert] = useState(false);
  const [b, setB] = useState(false);
  let total = 100;

  //   useEffect(() => {
  //     setInterval(() => setCompleted(Math.floor(Math.random() * 100) + 1), 2000);
  //   }, []);

  function addScore() {
    let n = (50 * 100) / total;
    let a = completed + n;
    if (a >= 100) {
      if (level + 1 >= 4) {
        setLevel(1);
      } else {
        setLevel(level + 1);
      }
      let temp = a - 100;

      setCompleted(temp);
      updateNumoxy();
      getO2(true);
    } else {
      setCompleted(a);
    }
  }
  function updateNumwater() {
    if (numwater - 5 <= 0) {
      if (numwater - 5 === 0) {
        addScore();
        setNumwater(numwater - 5);
      }
      setB(true);
      setAlert(true);
    } else {
      addScore();
      setNumwater(numwater - 5);
    }
  }
  function updateNumoxy() {
    setNumoxy(numoxy + 5);
  }
  function getO2(param) {
    setModalOShow(param);
  }
  return (
    <div className="Plant container-fluid" style={{ marginTop: "100px" }}>
      <Container>
        {/* Head */}
        <Row>
          <Col>
            <h3
              className="title__lv"
              style={{
                color: "#385723",
                fontFamily: "Raleway",
                textAlign: "left",
              }}
            >
              LV.{level}
            </h3>

            <ProgressBar bgcolor={"#00695c"} completed={completed} />
          </Col>
          <Col>
            {alert && (
              <div>
                <Alert show={alert} variant="danger">
                  Watertuning not enough Your watertuning is empty now —{" "}
                  <strong>Keep recycling</strong>
                </Alert>
              </div>
            )}
          </Col>
          <Col>
          <Container>
            <div className="container-m" padding="5px" style={{ background: "#B1D7A0",
                  border: "#B1D7A0" ,width:"120px" ,height:"100px", borderRadius:"8px", marginLeft:"5px"}}>
              <h2 className="title-menu container-m" color="green" >
                <Badge  className="title-menu container-m" pill variant="light">
                  <img alt="" src={water} width="30px" /> {numwater}
                </Badge>
              </h2>{" "}
              <h2 className="title-menu container-m" color="green">
                <Badge  className="title-menu container-m" pill variant="light">
                  <img alt="" src={o2} width="30px" /> {numoxy}
                </Badge>{" "}
              </h2>
            </div>
            </Container>
            {/* <ChipScore numOxy={numoxy} numWater={numwater} /> */}
          </Col>
        </Row>
        {/* tree */}
        <Row>
          <Col></Col>
          <Col>
            <img
              style={{ marginTop: "150px" }}
              width="250px"
              alt=""
              src={tree[level - 1]}
            />
          </Col>
          <Col></Col>
        </Row>
        {/* green */}
        <Row>
          <Col></Col>
          <Col>
            <>
              <Button
                style={{
                  background: "#B1D7A0",
                  border: "#B1D7A0",
                  marginTop: "30px",
                  color: "#385723",
                  fontWeight: "bold",
                  marginBottom: "20px",
                }}
                onClick={() => {
                  updateNumwater();
                  setModalShow(true);
                }}
                disabled={b}
              >
                GREEN ME
              </Button>

              <h5 className="small_cap">
                Use 5 <img alt="" src={water} width="50px" />
              </h5>

              <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => {
                  setModalShow(false);

                  //   setInterval(setModalShow(false), 5000);
                }}
              />
              {modalShow && (
                <div>
                  <MyVerticallyCenteredModalO2
                    show={modalOShow}
                    onHide={() => {
                      setModalOShow(false);
                      //   setInterval(setModalShow(false), 5000);
                    }}
                  />
                </div>
              )}
            </>
          </Col>
          <Col></Col>
        </Row>
      </Container>
    </div>
  );
}

export default Plant;
