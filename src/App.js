import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import {Route,Switch} from "react-router-dom";
import Home from "./Home";
import Plant from "./Plant";
import Recycle from "./Recycle";
import Reward from "./Reward";
import Ranking from "./Ranking";
import Navhead from "./components/Navhead.js";
import {withAuthenticator } from "@aws-amplify/ui-react";
import Amplify,{API}  from 'aws-amplify';
import {Auth} from 'aws-amplify';
// import {ThemeProvider} from '@material-ui/styles';
// import ControlledCarousel from "./components/Carouselhome.js";
// import Container from "react-bootstrap/Container";
// import Row from "react-bootstrap/Row";
// import Col from "react-bootstrap/Col";
// import MiniRank from "./components/MiniRank.js";
// import HomeMenu from "./components/HomeMenu";
// import {Switch,Route, NavLink} from "react-router-dom";

Amplify.configure({
  Auth:{
    identityPoolId: 'us-east-1:67a5613c-7964-45ad-b4a5-49c1daafe020',
    region:'us-east-1',
    userPoolId:'us-east-1_Xsa3O8Jux',
    userPoolWebClientId : 'g25mso6e3ab0fed3dbpnihu76'
  },
  API: {
    endpoints:[
      {
        name:"GreenmeAPI",
        endpoint:"https://5e8fm1jl67.execute-api.us-east-1.amazonaws.com"
      }
    ]
  }
});

function App() {
  
  
  // (async componentDidMount(){
  //   try {
  //     const user = await Auth.currentAuthenticatedUser();
  //   }catch(err){
  //     console.log(err);
  //   }
  // })
  return (
    <div className="App">
      <div><Navhead/></div>
      {/* componentDidMount();
      <p>{user}</p> */}
      
      <Switch>
      <Route exact path="/" component={Home}/>
      <Route  path="/plant" component={Plant}/>
      <Route  path="/recycle" component={Recycle}/>
      <Route  path="/reward" component={Reward}/>
      <Route  path="/ranking" component={Ranking}/>
      </Switch>
    </div>
  );
}



export default withAuthenticator(App);
