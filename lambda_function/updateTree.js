'use strict'
const AWS = require('aws-sdk');

AWS.config.update({ region: "us-east-1"});

exports.handler = async (event, context) => {
  const ddb = new AWS.DynamoDB({ apiVersion: "2012-10-08"});
  const documentClient = new AWS.DynamoDB.DocumentClient({ region: "us-east-1"});
    
  let responseBody = "";
  let statusCode = 0;
  const { username,level,score,h2o,o2 } = event.pathParameters;
  
  const params = {
    TableName: "Score_User",
    Key: {
        username : username
    },
    UpdateExpression: "set level = :n",
    ExpressionAttributeValues: {
      ":n": level
    },
    ReturnValues: "UPDATED_NEW"
    };
  
  try {
    const data = await documentClient.update(params).promise();
    responseBody = JSON.stringify(data);
    statusCode = 204;
  } catch (err) {
    responseBody = `Unable to update tree data: ${err}`;
    statusCode = 403;
  }

  const response = {
    statusCode: statusCode,
    headers: {
      "myHeader": "test"
    },
    body: responseBody
  };

  return response;
};
  



